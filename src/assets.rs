use super::*;
use bevy::audio::AudioSource;

pub(crate) struct GlobalAssets {
    pub text_style: TextStyle,
    pub menu: Handle<Image>,
    pub logo: Handle<Image>,
    pub charts: Vec<Chart>,
    pub music: Handle<AudioSource>,
    pub chops: Vec<Handle<AudioSource>>,
    pub miss: Handle<AudioSource>,
    pub bg: Handle<Image>,
    pub turntable: Handle<Image>,
    pub knife_left: Handle<Image>,
    pub knife_right: Handle<Image>,
    pub knife_down_left: Handle<Image>,
    pub knife_down_right: Handle<Image>,
    pub hearts: Vec<(Handle<Image>, Handle<Image>)>,
    pub bars: Vec<(Handle<Image>, Handle<Image>)>,
    pub cuts: Vec<Handle<Image>>,
}

impl GlobalAssets {
    pub(crate) fn load(asset_server: &AssetServer) -> Self {
        GlobalAssets {
            text_style: TextStyle {
                font: asset_server.load("Caveat/static/Caveat-Regular.ttf"),
                font_size: 256.,
                color: Color::BLACK,
            },
            menu: asset_server.load("menu.png"),
            logo: asset_server.load("logo.png"),
            charts: make_charts(&asset_server),
            music: asset_server.load("Banana.ogg"),
            chops: vec![
                asset_server.load("cut1.ogg"),
                asset_server.load("cut2.ogg"),
                asset_server.load("cut3.ogg"),
                asset_server.load("cut4.ogg"),
            ],
            miss: asset_server.load("miss.ogg"),
            bg: asset_server.load("paper.png"),
            turntable: asset_server.load("board.png"),
            knife_left: asset_server.load("knife-left.png"),
            knife_right: asset_server.load("knife-right.png"),
            knife_down_left: asset_server.load("knife-down-left.png"),
            knife_down_right: asset_server.load("knife-down-right.png"),
            hearts: vec![
                (asset_server.load("heart1.png"), asset_server.load("heart1-broken.png")),
                (asset_server.load("heart2.png"), asset_server.load("heart2-broken.png")),
                (asset_server.load("heart3.png"), asset_server.load("heart3-broken.png")),
            ],
            bars: vec![
                (asset_server.load("bar1b.png"), asset_server.load("bar1r.png")),
                (asset_server.load("bar2b.png"), asset_server.load("bar2r.png")),
                (asset_server.load("bar3b.png"), asset_server.load("bar3r.png")),
            ],
            cuts: vec![
                asset_server.load("cut1.png"),
                asset_server.load("cut2.png"),
                asset_server.load("cut3.png"),
            ],
        }
    }
}

pub(crate) struct Chart {
    pub asset: Handle<Image>,
    pub asset_success: Handle<Image>,
    pub cuts: Vec<(knife::Knife, f32)>,
}

fn make_charts_inner(descr: &str, asset_server: &AssetServer) -> Vec<Chart> {
    descr
        .split('\n')
        .map(|i| i.trim())
        .filter(|i| !i.is_empty())
        .map(|i| i.split_once(' ').unwrap())
        .map(|(path, rest)| Chart {
            asset: asset_server.load(&format!("{path}.png")),
            asset_success: asset_server.load(&format!("{path}-cut.png")),
            cuts: rest
                .split('/')
                .map(|i| i.trim())
                .map(|i| {
                    let c = i.chars().next().unwrap();
                    (
                        if c == 'l' {
                            knife::Knife::Left
                        } else {
                            knife::Knife::Right
                        },
                        i[c.len_utf8()..].parse::<f32>().unwrap() / BEATS_PER_CHART,
                    )
                })
                .collect(),
        })
        .collect()
}

#[cfg(target_arch = "wasm32")]
pub(crate) fn make_charts(asset_server: &AssetServer) -> Vec<Chart> {
    make_charts_inner(include_str!("../assets/charts"), asset_server)
}

#[cfg(not(target_arch = "wasm32"))]
pub(crate) fn make_charts(asset_server: &AssetServer) -> Vec<Chart> {
    make_charts_inner(
        &std::fs::read_to_string("assets/charts").unwrap(),
        asset_server,
    )
}
