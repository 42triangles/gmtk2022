use super::*;

const HEART_SIZE: f32 = 0.15 * WIDTH;

#[derive(Component)]
pub(crate) struct Heart {
    broken: bool,
    number: usize,
    broken_variant: Handle<Image>,
}

#[derive(Bundle)]
pub(crate) struct HeartBundle {
    #[bundle]
    sprite: SpriteBundle,
    heart: Heart,
    of_state: OfState,
}

impl HeartBundle {
    pub(crate) fn new(assets: &assets::GlobalAssets, number: usize) -> Self {
        HeartBundle {
            sprite: SpriteBundle {
                sprite: Sprite { custom_size: Some(Vec2::ONE * HEART_SIZE), ..Default::default() },
                transform: Transform::from_translation(
                    Vec3::new(-WIDTH + HEART_SIZE * (number as f32 + 0.5), -HEART_SIZE, 10.)
                ),
                texture: assets.hearts[number].0.clone(),
                ..Default::default()
            },
            heart: Heart { broken: false, number, broken_variant: assets.hearts[number].1.clone() },
            of_state: OfState,
        }
    }
}

pub(crate) fn update_hearts(
    mut hearts: Query<(&mut Handle<Image>, &mut Heart)>,
    state: Res<state::GameState>
) {
    let lives = in_state!(StageProgress { lives, .. } = *state => lives);

    for (mut img, mut heart) in hearts.iter_mut() {
        if !heart.broken && heart.number >= lives {
            heart.broken = true;
            *img = heart.broken_variant.clone();
        }
    }
}
