use super::*;

macro_rules! in_state {
    ($state:ident $(($($inparen:tt)*))? $({$($inbrace:tt)*})? = $expr:expr $(=> $res:expr)?) => {
        match $expr {
            crate::state::GameState::$state $(($($inparen)*))? $({$($inbrace)*})? => {
                ()
                $(; $res)?
            },
            _ => return,
        }
    };
}

pub(crate) use in_state;

#[derive(PartialEq, Eq)]
pub(crate) enum GameState {
    Menu,
    StageProgress {
        past_rotations: usize,
        last_start: Instant,
        score: usize,
        lives: usize,
        start: Instant,
    },
    ScoreScreen(Instant),
}

#[derive(Component)]
pub(crate) struct OfState;

pub(crate) fn clean(
    commands: &mut Commands,
    query: &Query<Entity, With<OfState>>,
    audio: &mut Option<Music>,
    assets: &Assets<bevy::audio::AudioSink>,
) {
    if let Some(audio) = audio.as_ref().and_then(|x| assets.get(&x.0)) {
        audio.stop();
    }

    for i in query.iter() {
        commands.entity(i).despawn();
    }
}
