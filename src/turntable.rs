use super::*;

#[derive(Component)]
pub(crate) struct Turntable;

#[derive(Component)]
pub(crate) struct TurntableEnter {
    pub(crate) from: Instant,
}

#[derive(Component)]
pub(crate) struct TurntableLeave {
    pub(crate) from: Instant,
}

#[derive(Component)]
pub(crate) struct SwapAssetOnSuccess(Handle<Image>);

pub(crate) fn turn_turntable(
    mut turntable: Query<(&mut Transform, Option<&TurntableLeave>), With<Turntable>>,
    state: Res<GameState>,
) {
    let last_start = in_state!(StageProgress { last_start, .. } = &*state => last_start);

    let rotation_time = rotation_time().as_secs_f32();
    let rot = rotation(
        (last_start
            .elapsed()
            .checked_sub(switch_time())
            .unwrap_or(Duration::ZERO))
        .as_secs_f32()
        .min(rotation_time)
            / rotation_time
            * MAX_ANGLE,
    );
    for mut i in turntable
        .iter_mut()
        .filter(|(_, leave)| leave.is_none())
        .map(|(i, _)| i)
    {
        i.rotation = rot;
    }
}

pub(crate) fn spawn_turntable(
    commands: &mut Commands,
    start: Instant,
    assets: &assets::GlobalAssets,
    index: usize,
) {
    let chart = &assets.charts[index.min(&assets.charts.len() - 1)];

    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                custom_size: Some(Vec2::new(1., 1.) * WIDTH),
                ..Default::default()
            },
            texture: assets.turntable.clone(),
            transform: Transform::default().with_translation(Vec3::new(0., 2. * WIDTH, 1.)),
            ..Default::default()
        })
        .insert(Turntable)
        .insert(OfState)
        .insert(TurntableEnter { from: start })
        .with_children(|commands| {
            commands
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        custom_size: Some(0.9 * Vec2::new(CUT_ASSET_ASPECT_XDY, 1.) * WIDTH),
                        ..Default::default()
                    },
                    texture: chart.asset.clone(),
                    transform: Transform::default().with_translation(Vec3::new(0., 0., 0.5)),
                    ..Default::default()
                })
                .insert(OfState)
                .insert(SwapAssetOnSuccess(chart.asset_success.clone()));

            for &i in &chart.cuts {
                commands.spawn_bundle(cuts::CutBundle::from_chart_entry(i, assets));
            }
        });
}

pub(crate) fn success_image(
    mut commands: Commands,
    cuts: Query<(Entity, &cuts::Cut, &Parent)>,
    mut to_be_replaced: Query<(&mut Handle<Image>, &SwapAssetOnSuccess, &Parent)>,
) {
    for (mut image, swap, parent) in to_be_replaced.iter_mut() {
        if cuts
            .iter()
            .filter(|(_, _, p)| **p == *parent)
            .all(|(_, cut, _)| cut.finished.is_some())
        {
            if *image != swap.0 {
                *image = swap.0.clone();
            }

            for (entity, _, _) in cuts.iter().filter(|(_, _, p)| **p == *parent) {
                commands.entity(entity).despawn();
            }
        }
    }
}

fn animate<T: std::ops::DerefMut<Target = Transform>>(
    source: Vec2,
    target: Vec2,
    entities: impl IntoIterator<Item = (Entity, T, Instant)>,
    mut on_end: impl FnMut(Entity),
) {
    for (entity, mut transform, from) in entities {
        let factor = from.elapsed().as_secs_f32() / switch_time().as_secs_f32();
        transform.translation = source
            .lerp(target, factor.min(1.))
            .extend(transform.translation.z);
        if factor >= 1. {
            on_end(entity);
        }
    }
}

pub(crate) fn enter(
    mut commands: Commands,
    mut enter: Query<(Entity, &mut Transform, &TurntableEnter)>,
) {
    animate(
        Vec2::new(0., 2. * WIDTH),
        Vec2::ZERO,
        enter.iter_mut().map(|(e, t, tte)| (e, t, tte.from)),
        |entity| {
            commands.entity(entity).remove::<TurntableEnter>();
        },
    );
}

pub(crate) fn leave(
    mut commands: Commands,
    mut enter: Query<(Entity, &mut Transform, &TurntableLeave)>,
) {
    animate(
        Vec2::ZERO,
        Vec2::new(0., -2. * WIDTH),
        enter.iter_mut().map(|(e, t, ttl)| (e, t, ttl.from)),
        |entity| commands.entity(entity).despawn_recursive(),
    );
}
