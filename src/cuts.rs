use super::*;
use rand::prelude::*;

#[derive(Component)]
pub(crate) struct Cut {
    pub(crate) finished: Option<f32>,
    pub(crate) for_: knife::Knife,
    at: f32,
    pub(crate) active: bool,
}

pub(crate) fn do_cut(
    mut state: ResMut<GameState>,
    mut cuts: Query<(&mut Cut, &mut Handle<Image>)>,
    input: Res<Input<knife::Knife>>,
    config: Res<StageConfig>,
    audio: Res<Audio>,
    assets: Res<assets::GlobalAssets>,
    mut game_over: EventWriter<GameOver>,
) {
    let (score, lives, last_start) = in_state!(
        StageProgress { score, lives, last_start, .. } = &mut *state => (score, lives, last_start)
    );

    let at = (last_start.elapsed().as_secs_f32() * BPS - WAIT_BEATS) / BEATS_PER_CHART;
    let judgement = config.cut_max_diff;

    for knife in knife::Knife::all() {
        // TODO: Limit to cuts of the leaving board if there are two
        //Duration::from_secs_f32((WAIT_BEATS + angle * BEATS_PER_CHART) / BPS),
        if input.just_pressed(knife) {
            let closest = cuts
                .iter_mut()
                .filter(|(cut, _)| cut.active)
                .filter(|(cut, _)| cut.for_ == knife)
                .filter(|(cut, _)| cut.finished.is_none())
                .map(|(cut, draw)| ((cut.at - at).abs(), cut, draw))
                .filter(|&(diff, _, _)| diff <= judgement)
                .min_by(|l, r| l.0.partial_cmp(&r.0).unwrap());

            if let Some((diff, mut cut, mut texture)) = closest {
                cut.finished = Some(diff);
                *texture = assets.cuts[thread_rng().gen_range(0..assets.cuts.len())].clone();

                *score += ((config.cut_max_diff - diff) / config.cut_max_diff * 100.) as usize;

                use rand::prelude::*;
                audio.play(assets.chops[thread_rng().gen_range(0..assets.chops.len())].clone());
            } else {
                audio.play(assets.miss.clone());
                *lives -= 1;
                if *lives == 0 {
                    game_over.send(GameOver { stop_music: true });
                }
            }
        }
    }
}

#[derive(Bundle)]
pub(crate) struct CutBundle {
    #[bundle]
    line: SpriteBundle,
    cut: Cut,
    in_game: OfState,
}

impl CutBundle {
    pub(crate) fn from_chart_entry((knife, factor): (knife::Knife, f32), assets: &assets::GlobalAssets) -> Self {
        let angle = factor * MAX_ANGLE;

        let width = 0.02 * WIDTH;

        let assets = &assets.bars[thread_rng().gen_range(0..assets.bars.len())];

        CutBundle {
            line: SpriteBundle {
                sprite: Sprite {
                    custom_size: Some(Vec2::new(CUT_ASPECT_XDY * width, width)),
                    ..Default::default()
                },
                transform: Transform::from_rotation(rotation(0.25 * consts::TAU - angle))
                    .with_translation(
                        (Transform::from_rotation(rotation(-angle)) * Vec3::new(knife.pos() * RADIUS * WIDTH, 0., 0.)).truncate().extend(1.)
                    ),
                texture: match knife {knife::Knife::Left => &assets.0, knife::Knife::Right => &assets.1}.clone(),
                ..Default::default()
            },
            /*line: GeometryBuilder::build_as(
                &shapes::Line(side_a, side_b),
                DrawMode::Stroke(StrokeMode::new(knife.color(), 0.005 * WIDTH)),
                Transform::default()
                    .with_rotation(rotation(-angle))
                    .with_translation(Vec3::new(0., 0., 1.)),
            ),*/
            cut: Cut {
                finished: None,
                for_: knife,
                at: factor,
                active: true,
            },
            in_game: OfState,
        }
    }
}
