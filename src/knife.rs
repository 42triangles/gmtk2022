use super::*;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Component)]
pub(crate) enum Knife {
    Left,
    Right,
}

impl Knife {
    pub(crate) fn all() -> impl Iterator<Item = Knife> {
        [Knife::Left, Knife::Right].into_iter()
    }

    pub(crate) fn select_asset(self, assets: &assets::GlobalAssets) -> &Handle<Image> {
        match self {
            Knife::Left => &assets.knife_left,
            Knife::Right => &assets.knife_right,
        }
    }

    pub(crate) fn select_down_asset(self, assets: &assets::GlobalAssets) -> &Handle<Image> {
        match self {
            Knife::Left => &assets.knife_down_left,
            Knife::Right => &assets.knife_down_right,
        }
    }

    pub(crate) fn pos(self) -> f32 {
        match self {
            Knife::Left => 0.2,
            Knife::Right => 0.4,
        }
    }

    pub(crate) fn anchor(self) -> Anchor {
        match self {
            Knife::Left => Anchor::CenterRight,
            Knife::Right => Anchor::CenterLeft,
        }
    }

    pub(crate) fn keys(self) -> impl Iterator<Item = KeyCode> {
        match self {
            Knife::Left => [KeyCode::Left, KeyCode::A].into_iter(),
            Knife::Right => [KeyCode::Right, KeyCode::D].into_iter(),
        }
    }
}

pub(crate) fn cut_visuals(
    mut knife: Query<(&mut Sprite, &mut Handle<Image>, &Knife)>,
    input: Res<Input<Knife>>,
    assets: Res<assets::GlobalAssets>,
) {
    for (mut sprite, mut texture, knife) in knife.iter_mut() {
        let (anchor_to_use, texture_to_use) = if input.pressed(*knife) {
            (Anchor::Center, knife.select_down_asset(&assets))
        } else {
            (knife.anchor(), knife.select_asset(&assets))
        };

        // Try not to trigger any change detection unless we actually change something
        if sprite.anchor.as_vec() != anchor_to_use.as_vec() {
            sprite.anchor = anchor_to_use;
        }
        if *texture != *texture_to_use {
            *texture = texture_to_use.clone();
        }
    }
}

const KNIFE_HEIGHT: f32 = 2. * KNIFE_UP_FACT * RADIUS * WIDTH;

#[derive(Bundle)]
pub(crate) struct KnifeBundle {
    #[bundle]
    sprite: SpriteBundle,
    knife: Knife,
    of_state: OfState,
}

impl KnifeBundle {
    pub(crate) fn for_(knife: Knife, assets: &assets::GlobalAssets) -> Self {
        KnifeBundle {
            sprite: SpriteBundle {
                sprite: Sprite {
                    custom_size: Some(Vec2::new(KNIFE_UP_ASPECT_XDY * KNIFE_HEIGHT, KNIFE_HEIGHT)),
                    anchor: knife.anchor(),
                    ..Default::default()
                },
                texture: knife.select_asset(assets).clone(),
                transform: Transform::default().with_translation(Vec3::new(
                    knife.pos() * RADIUS * WIDTH,
                    0.,
                    3.,
                )),
                ..Default::default()
            },
            knife,
            of_state: OfState,
        }
    }
}
